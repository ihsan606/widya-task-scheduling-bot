import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import axios from 'axios';
import { DiscordClientProvider, On, Once } from 'discord-nestjs';
import { Message } from 'discord.js';
import { DatabaseService } from './db.service';

/**
 *   "etxbn8htbfrwtrx7j7wnc4goah", // gibran
  "xwhgt796y7bw5yqfpz53fjmcpo" // ihsan
 */

@Injectable()
export class BotGateway {
  private readonly logger = new Logger(BotGateway.name);
  private readonly workdays: number[] = [1, 2, 3, 4, 5]; // Hari Senin-Jumat
  private holidays: Holiday[] = []; // Simpan daftar hari libur di sini

  private async fetchHolidaysFromApi() {
    const current = new Date();
    try {
      const response = await axios.get(
        `https://api-harilibur.vercel.app/api?month=${
          current.getMonth() + 1
        }&year=${current.getFullYear()}`,
      );
      this.holidays = response.data;
      this.logger.log(this.holidays);
    } catch (error) {
      console.error('Error fetching holidays:', error.message);
    }
  }

  private isWorkday(date: Date): boolean {
    const dayOfWeek = date.getDay();
    const isHoliday = this.checkHoliday(date);
    this.logger.log(isHoliday, 'libur');

    return this.workdays.includes(dayOfWeek) && !isHoliday;
  }

  private checkHoliday(date: Date): boolean {
    const formattedDate = date.toISOString().substr(0, 10); // Ubah format tanggal menjadi YYYY-MM-DD
    this.logger.log(formattedDate);
    return this.holidays.some(
      (holiday) =>
        holiday.holiday_date === formattedDate &&
        holiday.is_national_holiday === true,
    );
  }

  private listTask = [
    {
      id: 763095989451358288n,
      username: 'ihsan',
      tasks: [
        {
          id: 1,
          kd_task: 'TASK03',
          name: 'buat endpoint create product',
          isDone: true,
        },
        {
          id: 2,
          kd_task: 'TASK04',
          name: 'buat endpoint edit product',
          isDone: false,
        },
      ],
    },
    {
      id: 1022118831189151855n,
      username: 'Anjeli',
      tasks: [
        {
          id: 3,
          kd_task: 'TASK01',
          name: 'buat endpoint create task',
          isDone: false,
        },
        {
          id: 4,
          kd_task: 'TASK02',
          name: 'buat endpoint edit task',
          isDone: false,
        },
      ],
    },
  ];

  getUncompleteTask() {
    return this.listTask.map((user) => ({
      ...user,
      tasks: user.tasks.filter((task) => !task.isDone),
    }));
  }

  constructor(
    private readonly databaseService: DatabaseService,
    private readonly discordProvider: DiscordClientProvider,
  ) {
    console.log(this.getUncompleteTask());
    this.fetchHolidaysFromApi();
  }

  @Once({ event: 'ready' })
  onReady(): void {
    this.logger.log(`Logged in as${this.discordProvider.getClient().user.tag}`);
  }

  @On({ event: 'messageCreate' })
  async repy(message: Message): Promise<void> {
    const content = message.content.trim();
    if (content.startsWith('!task')) {
      const args = content.slice('!task'.length).trim().split(/ +/);
      const subCommand = args.shift().toLowerCase();

      if (subCommand === 'list') {
        // this.sendTaskList(message.channelId);
      } else if (subCommand === 'done') {
        await message.reply(`Terimakasih ${message.author.displayName}`);
        const taskIndex = args[0]; // Menangkap nomor tugas yang diubah status
        // if (taskIndex && this.tasks[Number(taskIndex) - 1]) {
        //   this.tasks[Number(taskIndex) - 1].status = 'selesai';
        //   await message.reply(
        //     `Tugas No. ${taskIndex} telah diubah status menjadi selesai.`,
        //   );
        //   const taskList = this.tasks
        //     .map((task, index) => `${index + 1}. ${task.name} - ${task.status}`)
        //     .join('\n');
        //   await message.reply(`Daftar Tugas \n ${taskList}`);
        // } else {
        //   await message.reply('Nomor tugas tidak valid.');
        // }
      }
    }
  }

  async sendMessage(isWorkday: boolean): Promise<void> {
    const webhookUrl =
      'https://discord.com/api/webhooks/1141787983238942812/MloBET6jbS-XiUfLiLnS--pxt3tbJJIo4YuRclxhCeXScX-stmSmyi-7GaXKHOP8Q81g';
    try {
      const now = new Date();
      const taskList = this.getUncompleteTask()
        .map(
          (task, index) =>
            `${index + 1}. <@${task.id}>  tasks: \n\n > ${task.tasks
              .map((item) => `${item.name} [${item.kd_task}]`)
              .join('\n> ')}`,
        )
        .join('\n');
      let embed;

      if (isWorkday) {
        embed = {
          color: 0x3498db,
          title: 'Reminder Tugas',
          description: `Halo, ini adalah pengingat untuk tugas Anda.`,
          fields: [
            { name: 'Tugas', value: taskList },
            { name: 'Status', value: 'Belum Selesai' },
            {
              name: 'Petunjuk',
              value:
                'Untuk ubah status task menjadi done , gunakan format berikut \n ``` !task done [kodetask] ```',
            },
          ],
        };
      } else {
        const holiday = this.holidays.find(
          (holiday) => holiday.holiday_date === now.toISOString().substr(0, 10),
        );
        embed = {
          color: 0x3498db,
          title: `Selamat  ${holiday.holiday_name} 💤 👨‍👩‍👦`,
          description: `Tidak ada tugas yeay, selamat hari libur `,
        };
      }

      const payload = {
        username: 'TaskRemainder',
        embeds: [embed],
      };
      const response = await axios.post(webhookUrl, payload);
      console.log('Webhook called:', response.data);
    } catch (error) {
      console.error('Error calling webhook:', error.message);
    }
  }

  @Cron(CronExpression.EVERY_30_SECONDS)
  sendToDiscord() {
    const today = new Date();
    this.sendMessage(this.isWorkday(today));
  }

  @Cron(CronExpression.EVERY_30_SECONDS)
  async getExampleData(): Promise<any> {
    const query = 'SELECT * FROM users';
    const result = await this.databaseService.executeRawQuery(query);
    this.logger.log(result);
  }
}

interface Holiday {
  holiday_date: string;
  holiday_name: string;
  is_national_holiday: boolean;
}
