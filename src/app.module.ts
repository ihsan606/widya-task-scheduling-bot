import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ScheduleModule } from '@nestjs/schedule';
import { DiscordModule } from 'discord-nestjs';
import { GatewayIntentBits } from 'discord.js';
import { BotGateway } from './bot.gateway';
import { DatabaseService } from './db.service';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot(),
    ScheduleModule.forRoot(),
    DiscordModule.forRootAsync({
      useFactory: () => ({
        token:
          'MTE0MTc3OTA4MTA4NDU1MTM0Mg.GvSiNI.w7Tmix-KaxE-0jSvNPJ1YZjZuAptvL29-ZnwPM',
        commandPrefix: '%',
        intents: [
          GatewayIntentBits.Guilds,
          GatewayIntentBits.GuildMessages,
          GatewayIntentBits.MessageContent,
        ],
      }),
    }),
  ],
  controllers: [AppController],
  providers: [AppService, BotGateway, DatabaseService],
})
export class AppModule {}
